import re

def check_password(*args):
   	# Checks the passwords against criteria
	# password criteria
	criteria = re.compile(r'[\s*a-zA-Z0-9]+[#|@|$]+')
	matching_passwords = []
	for item in args:
		if 6 <= len(item) <= 12:
			if re.search(criteria, item):
				matching_passwords.append(item)

	# returns comma separated list of valid passwords
	return ','.join(matching_passwords)
