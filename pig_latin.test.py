import unittest
from pig_latin import pig_latin

class TestPigLatin(unittest.TestCase):
	def test_pig_latin(self):
		wordlist = ['will', 'dog', 'category', 'andela', 'elecrician']
		actual_list = []
		for word in wordlist:
			actual_list.append(pig_latin(word))

		expected_list = ['illway', 'ogday', 'ategorycay', 'andelaway', 'elecricianway']

		self.assertListEqual(actual_list, expected_list)

if __name__ == '__main__':
	unittest.main()
