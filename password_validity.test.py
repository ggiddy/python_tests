import unittest
from password_validity import check_password

class CheckPassword(unittest.TestCase):
	def test_password_valid(self):
		actual = check_password('1qA$sdf', 'sdfsdf', '3fG#bd', '2gS @vsd', '3fG#bdasdfsadfsadfsdfsdfsdf', '1qA$')
		expected = ','.join(['1qA$sdf', '3fG#bd', '2gS @vsd'])

		self.assertEqual(actual, expected)

if __name__ == '__main__':
	unittest.main()
