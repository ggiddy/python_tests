import re

def first_vowel(word):
	criteria = re.compile('^[aeiouAEIOU]')
	pos = 0
	while pos < len(word) - 1:
		if re.match(criteria, word[pos]):
			return pos
		pos = pos + 1

def pig_latin(word):
	# if word starts with consonant, take consonants before vowel and put them
	# at the end of word and add `ay`
	pos = first_vowel(word)

	if pos == 0:
		return (word[0:] + 'way')
	else:
		return (word[pos:] + word[0:pos] + 'ay')
